# Improvement Roadmap
## 1. Table Improvements
- Header Tooltips: Depending on who this table is targeted at, adding tooltips to the headers with the information provided in the 'Glossary' of app.js might be a good first step. It would make the table more approachable to someone who is just beginning to interact with the dashboard.
- Filtering: Searching and filtering data is pretty much universally important. I would definitely add this functionality ASAP to make this data easier to zoom in to.
- More details view: Allow for clicks on each vehicle to get even more information about it, maybe in a pop up card or a new panel. Give users the ability to edit certain fields, if we want that functionality (I assume we'd need to be very careful, if we even give them any editting powers at all). 

## 2. Graph Improvements
- Adding graph axes, labels, a better legend, and other basic info: Step 0. Right now, it's pretty much impossible at a glance to make sense of the data. My first thought was, What am I even looking at here?
- Adding multiple graphs or changing the scaling: Does it really make sense to have these 3 data all on one graph, when their scales are vastly different? Perhaps a logarithmic graph would allow the user to at least get some sense of comparison, instead of just completely flatlining trips when it's graphed alongside miles, and even more extremely, alongside GHG.
- Filtering: Allow the user to zoom in on a specific time range, and get super granular, maybe even look at individual trips from a month where mileage/GHG really spiked, to better understand what happened.

## 3. Map Improvements
- Density tracking: Aggregate parking instances for a specific location in an array (or similar) and change the colors of the markers depending upon how often the vehicle was parked there. IE. ranging from light blue to dark blue - just helping the user get a quick sense of the vehicle's frequent parking spots. This is already sort of happening because of the transparent markers, but it's more like a side effect not intended behavior. this would also help with...
- Multiple parking instances display: Owners have habits. Some of these locations have been parked at 5,10,20 times. Showing a stack or carousel of cards when a particular location is clicked might be helpful. I think this would get into some more complex logic since right now the markers don't really know the others exist, and the info display logic works only for the marker the user clicked.
- Show other info about the vehicle: Cross-reference the VIN with the data from the emissions table to show some 'quick info' about a vehicle (assuming that we would eventually also be sent data points from multiple vehicles). Bringing in more state management might help here to minimize API calls + keep things clean.
- Info view Close button: Add a close button that collapses the card and deselects the currently selected marker.
- Minor: Clean up the formatting on duration - don't show s's if singular, don't show if 0, etc.


## Other General Improvements
- Error handling across the board: I opted to omit error handling for time's sake. There are tons of places for things to go sideways here, and almost none of the holes are plugged.
- State management: Would be more and more important as we add more data tabs and views within those tabs.
- Responsive design: Obviously.