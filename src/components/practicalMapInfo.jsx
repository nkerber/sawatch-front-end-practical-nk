const PracticalMapInfo = ({ marker }) => {
    const options = {
        year: "numeric",
        month: "long",
        day: "numeric",
        hour: "2-digit",
        minute: "2-digit",
        second: "2-digit",
        timeZoneName: "short",
        timeZone: "America/Denver" // Set the timeZone option to MDT
    };
    let formattedStart = "";
    let formattedStop = "";

    formattedStart = new Intl.DateTimeFormat("en-US", options).format(new Date(marker.utcStart));
    formattedStop = new Intl.DateTimeFormat("en-US", options).format(new Date(marker.utcStop));

    return (
        <div className="info-card">
            <div> <span className="data-label">Vehicle VIN:</span> {marker.vin}</div>
            {marker.address != "Street Address Not Found" && <div> <span className="data-label">Address:</span> {marker.address}</div>}
            <div> <span className="data-label">Coordinates:</span> ({marker.lat},{marker.lon})`</div>
            <div> <span className="data-label">Started:</span> {formattedStart}</div>
            <div> <span className="data-label">Ended:</span> {formattedStop}</div>
            <div> <span className="data-label">Duration:</span> {Math.floor(marker.duration / 3600)} hours, {Math.floor((marker.duration % 3600)/60)} minutes, {marker.duration % 60} seconds</div>
            <div> <span className="data-label">Duty Cycle:</span> {marker.dutyCycle}</div>
        </div>
    )
}

export default PracticalMapInfo