import React, { useState } from "react";
import { CircleMarker, MapContainer, TileLayer, Tooltip} from "react-leaflet";
import PracticalMapInfo from "./practicalMapInfo";

export default function PracticalMap(props){

  //@TODO add a context view, and highlight the marker currently selected.
  const [showMapInfo, setShowMapInfo] = useState(false);
  const [mapInfo, setMapInfo] = useState({});
  const [selectedMarker, setSelectedMarker] = useState(null);

  const { data } = props;   
  const vinNumbers = new Set();

  const handleMarkerClick = (marker) => {
    selectedMarker && selectedMarker.setStyle({color:'black'}); // if there is a previously selected marker, set it back to black
    marker.setStyle({color:'blue'}); // set the newly selected marker's color
    setSelectedMarker(marker); //keep record of the current marker in state
  }

  function parkingMarks() {
    
    return data.map(pl => {
      !(pl.vin in vinNumbers) && vinNumbers.add(pl.vin); // get the VIN numbers for the vehicles selected
      return(
        <CircleMarker
          key={pl.uuid}
          center={[pl.lat, pl.lon]}
          color={'black'}          
          radius={10}
          eventHandlers={{
            click: (e) => {
              setMapInfo(pl);
              setShowMapInfo(true);
              handleMarkerClick(e.target);
            }
          }}
        >
          <Tooltip>
            {pl.address === "Street Address Not Found" ?
              `Coordinates: (${pl.lat},${pl.lon})` : 
              `Location: ${pl.address}`}
          </Tooltip>
        </CircleMarker>
      );
    });
  };

  function getWelcome() {
    const response = vinNumbers.size > 1 ? 
      `You're viewing the parking history for vehicles ${[...vinNumbers()].join(', ')}.` :
      `You're viewing the parking history for vehicle ${[...vinNumbers][0]}.`;
    return response;
  }

  return(
    <div className = "map-panel">
      <MapContainer
        className = "map-container"
        center={[39.79208,-105.08285]}
        zoom={13}
      >
        <TileLayer
          attribution='&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors &copy; <a href="https://carto.com/attributions">CARTO</a>'
          url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
          maxZoom={20}
        />
        {parkingMarks()}
      </MapContainer>
      <div className="map-info">
        <p className="map-info-title">{getWelcome()}</p>
        {showMapInfo ? <PracticalMapInfo marker = {mapInfo} /> : (
        <>
          <p className = "map-info-suggestion">
            Click on any marker to get more information about that parking instance.
          </p>
        </>
        )
      }
        
      </div>
    </div>);
}
