import { Route, Switch, NavLink, BrowserRouter as Router } from "react-router-dom";
import React, { useState, useEffect } from "react";
import PracticalMap from "./components/practicalMap";
import PracticalGraph from "./components/practicalGraph";
import PracticalTable from "./components/practicalTable";
import './App.css';
import "leaflet/dist/leaflet.css";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTableList, faChartLine, faMapLocationDot} from "@fortawesome/free-solid-svg-icons";

/*
                   *** GLOSSARY ***

  This is for applicants and not something we'd generally
  put into production code.

  GHG - Green House Gas emissions
  NOX - A group of Nitrogen Oxide compounds (included in GHG).
  CO - Carbon Monoxide, not a GHG, but not great.
  PM 10 - Particulate matter sized around 10 micrometers.
          A human hair is 70-100 micrometers in width.
          This dusty stuff gets stuck in the nose and upper respitory system.
  PM 2.5 - Particulate matter sized around 2.5 micrometers.
           It's more likely to get deep inside the lungs.
           Also not great.
  Emission Ratio - This is a bespoke measurment of emissions / miles
          Vehicles are meant to be driven. We don't want to ping them
          for doing their job, but we do want to differentiate cleaner
          miles from dirtier mile, which is why wer surface this ratio.
  Fuel Economy - Estimate miles per gallon.
*/

const API_URL = 'https://dev-api.sawatchlabs.com/';
const EMISSIONS_URL = `${API_URL}/emissionsForPractical`;
const MILAGE_URL = `${API_URL}/milageDataforPractical`;
const PARKING_URL = `${API_URL}/parkingEventsForPractical`;

const tableColumns = [
      {Header: "Asset ID", accessor: "assetId", sortType: "basic"},
      {Header: "Class", accessor: "vehicleClass", sortType: "basic"},
      {Header: "Miles", accessor: "miles", sortType: "basic"},
      {Header: "Emission Ratio", accessor: "emit", sortType: "basic"},
      {Header: "Fuel Economy", accessor: "fuelEcon", sortType: "basic"},
      {Header: "GHG tons", accessor: "ghg", sortType: "basic"},
      {Header: "NOX lbs", accessor: "nox", sortType: "basic"},
      {Header: "CO lbs", accessor: "co", sortType: "basic"},
      {Header: "PM 10 lbs", accessor: "pm10", sortType: "basic"},
      {Header: "PM 2.5 lbs", accessor: "pm25", sortType: "basic"},
      {Header: "Year", accessor: "year", sortType: "basic"},
      {Header: "Make", accessor: "make", sortType: "basic"},
      {Header: "Model", accessor: "model", sortType: "basic"}
      ]

function App() {
  // pre-defined shape
  const [emissions, setEmissions] = useState([]);
  const [milage, setMilage] = useState([]);
  const [parking, setParking] = useState([]);

  const PracticalLink = ({id, label,icon}) =>{
    return <NavLink to={`/${id}`} id={id} className="app-button" ><FontAwesomeIcon icon={icon}/>{label}</NavLink>;
  };

  useEffect( () => {
    const loadData = async () => {
      const emissionsFromAPI = await fetchEmissions();
      const milageFromAPI = await fetchMilage();
      const parkingFromAPI = await fetchParking();
      setEmissions(emissionsFromAPI);
      setMilage(milageFromAPI);
      setParking(parkingFromAPI);
    };
    loadData();
  },[]);
  
  // Fetch the emissions data for the table
  const fetchEmissions = async () => {
    const rawResponse = await fetch(EMISSIONS_URL);
    const jsonResponse = await rawResponse.json();
    transformEmissionData(jsonResponse.data)
    return jsonResponse.data;
  }
  
  // Fetch the milage data for the graph
  const fetchMilage = async () => {
    const rawResponse = await fetch(MILAGE_URL);
    const jsonResponse = await rawResponse.json();
    return jsonResponse.data;
  }
  
  // Fetch the parking data for the map
  const fetchParking = async () => {
    const rawResponse = await fetch(PARKING_URL);
    const jsonResponse = await rawResponse.json();
    return jsonResponse.data;
  }

  // Transform the emissions data before sending it to the table, to fulfill Part 2.1
  const transformEmissionData = async (data) => {
    data.forEach(
      (vehicle) => {
        !vehicle.assetId && (vehicle.assetId = vehicle.vin); // If the vehicle does not have an asset ID, set its asset ID to the vehicle's VIN
        vehicle.make = vehicle.make.replace(/(^|-| )([a-z])/g, (match, group1, group2) => group1 + group2.toUpperCase()); // in this regex replace, we capture every instance of a lowercase letter (group2) at the beginning of the string, or after a hyphen or space (group1), and capitalize it wthin the callback function by replacing the entire match with the first target group and the second, capitalized, target group
      }
    )
  }

  return (
    <Router>
      <div className="app-wrapper">
        <nav className="app-button-wrapper">

          <PracticalLink id={'table'} label={'Emissions Table'} icon = {faTableList}/>
          <PracticalLink id = {'map'} label={'Map'} icon = {faMapLocationDot}/>          <PracticalLink id = {'graph'} label={'Graph'} icon = {faChartLine}/>

        </nav>
        <div className="app-tab-wrapper">
          <Switch>
            <Route path="/table" render={(props) => (<PracticalTable columns={tableColumns} data={emissions} />)} />
            <Route exact path="/map" render={(props) => (<PracticalMap data={parking} />)} />
            <Route exact path="/graph" render={(props) => (<PracticalGraph data={milage} />)} />
          </Switch>
        </div>
      </div>
    </Router>
  );
}

export default App;
