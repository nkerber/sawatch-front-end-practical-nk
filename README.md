# Sawatch Labs Front End Practical Project

Our intention here is to get a sense of how you approach code and interface problems with data and interfaces that are reasonably close to what we work with every day. We've simplified these interfaces as domain expertise is not a requirement, and we've left them intentionally awful in order to maximize the areas in which you can improve them.

This project has been built with React libraries that we use in production. Those being React Table 7, React Leaflet and React ChartJS-2.

https://thewidlarzgroup.com/react-table-7/<br>
https://www.npmjs.com/package/react-chartjs-2<br>
https://react-leaflet.js.org/<br>

React Leaflet and React ChartJS-2 are wrappers for the libraries Leaflet and Chart.js.

https://www.chartjs.org/docs/latest/
https://leafletjs.com/

To get started, clone the repo down to your local machine and create a private repo. We use Gitlab, so it'll be easiest if you make the repo there.

Once cloned, you'll want to install the dependencies via:   
    `npm install`

And then start the development server via:   
    `npm start`


# The requirements are as follows:

## Fetch the data:   
In the App.js file you'll find 3 defined URLs. Hit each of those end points to fetch the data to be consumed by the app. Each end point provides the data to be consumed by one of the tabs - emissions data is consumed by the table, milage data is consumed by the graph and parking data is consumed by the map.

We have provided some basic code for the table, graph and map. As such, once you fetch the data and set it to the (pre-defined) state object, the table, graph and map will all populate.

You will see that the table view has tailpipe and GHG data (a glossary of which is available at the top of the App.js file), the graph view has data around emissions, trips and milage and the map view shows parking location data.

Note that you will not be able to proceed to the subsequent requirements until you have fetched the data and set it to state.

## Fix the Table   
### Tidy the data
- For any rows where there is no 'Asset ID', write some code to sub in the 'VIN';
- Update the 'Make' to have an uppercase first letter and lower case following letters. Where there is a hyphen, the first letter following the hyphen should be uppercase.

### Build out the table   
- remove the 'VIN' column from the table;
- make each column sortable (using react-table built in functionality);
- style the table (as time allows).

## Improve the interface   
We'd like you to select either the map or the graph (only one) and work on one or two areas to improve the interface in that tab. This can be anything at all spanning the front-end, including improving user experience, usability, styling, presentation of data.
We have no preference on whether it be the map or graph. We've intentionally made a fairly large surface area that can be improved depending on your familiarity with the different libraries and domains.

## Outline additional changes   
Please don't spend more than two hours on this project. We know you could spend 6-8 more hours on this and radically improve every aspect of the project, but don't. We're really just trying to maximize your options, not waste your time.

Instead of spending the hours, we ask you write a brief roadmap of what you'd do given an additional 4 hours. The roadmap should not be comprehensive. There are at least 100 areas where this project be improved and expanded upon. We just want you to identify the next 3 or 4 areas you'd improve and what you'd do, given more time. A few sentences for each of the areas is sufficient.

With the exception of the provided URLs and React libraries, please feel free to change anything in this repo. You can add, delete, move and modify any code you wish.

When you're finished, please invite @mhelm and @remclay to the cloned repo as 'Developers'. We'll review and be in touch within a couple of days.

-Thanks,

Matt
